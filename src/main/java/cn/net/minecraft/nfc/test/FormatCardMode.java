package cn.net.minecraft.nfc.test;

import cn.net.minecraft.nfc.data.Block;
import cn.net.minecraft.nfc.data.Key;
import cn.net.minecraft.nfc.data.MifareClassicType;
import cn.net.minecraft.nfc.device.ACR122;
import cn.net.minecraft.nfc.protocol.MifareClassic;
import cn.net.minecraft.nfc.util.mfocConverter;

import javax.smartcardio.Card;
import javax.smartcardio.CardException;

import static cn.net.minecraft.nfc.util.ByteArrayUtil.bytesToHex;

/**
 * ***********************************************
 * Created by Sola on 2014/11/1.
 * Don't modify this source without my agreement
 * ***********************************************
 */
public class FormatCardMode {

	public static final String keyPath = "H:\\mfocGUI_v29\\compiled\\Keys\\";

	public static final Block defaultKeyBlock = new Block("FFFFFFFFFFFFFF078069FFFFFFFFFFFF");

	public static void main(String[] args) throws Exception {
		mfocConverter.keyPath = keyPath;
		ACR122.initTerminal();
		root:
		while (true) {
			try {
				Card card = ACR122.getAvailableCard();
				System.out.println("=====New Card Detected=====");
				MifareClassic m1 = new MifareClassic(card);
				byte[] UID = m1.getUID();
				System.out.println("Card UID: " + bytesToHex(UID));

				Key[] keyA = mfocConverter.convertKeyFile(bytesToHex(UID), MifareClassic.KEY_A);
				Key[] keyB = mfocConverter.convertKeyFile(bytesToHex(UID), MifareClassic.KEY_B);
				System.out.print("Writing Data : ");
				int blockCount = m1.getType() == MifareClassicType.MIFARE_CLASSIC_1K ? 64 : 256;
				for (int i = 1; i < blockCount; i++) { // Skip UID Block
					int sectorIndex = Block.getSectorIndex(i);
					if (m1.authBlock(keyA[sectorIndex], i, MifareClassic.KEY_A) || m1.authBlock(keyB[sectorIndex], i, MifareClassic.KEY_B)) {
						if (Block.isControlBlock(i)) {
							m1.writeBlock(i, defaultKeyBlock);
						} else {
							m1.writeBlock(i, Block.genDefault());
						}
					} else {
						System.out.println("Failed! (authentication failed) (Block:" + i + ")");
						sendErrorLED(m1);
						continue root;
					}
				}
				System.out.println("Done!");
				sendBuzzer(m1);
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("Error: Internal Error!");
			} finally {
				endOperation();
			}
		}
	}

	public static void sendBuzzer(MifareClassic m1) {
		m1.controlLED(0b00100000, 1, 2, 2, 1);
	}

	public static void sendErrorLED(MifareClassic m1) {
		m1.controlLED(0b01100000, 1, 1, 10, 1);
	}

	public static void endOperation() throws CardException {
		System.out.println("Waiting for removing the card...");
		ACR122.waitForAbsent(0);
		System.out.println("=====Card Operation End=====");
	}

}
