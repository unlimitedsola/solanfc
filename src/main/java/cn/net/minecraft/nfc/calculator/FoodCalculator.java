package cn.net.minecraft.nfc.calculator;

import static cn.net.minecraft.nfc.util.ByteArrayUtil.hexToBytes;

/**
 * ***********************************************
 * Created by Sola on 2014/7/1.
 * Don't modify this source without my agreement
 * ***********************************************
 */
public class FoodCalculator {

	public static byte[] calculateKey(byte[] uid) {
		final byte[] KEYGEN = hexToBytes("38303830");
		byte[] keyA = new byte[6];
		for (int j = 0; j < 4; j++) {
			keyA[3 - j] = (byte) (uid[j] ^ KEYGEN[j]);
		}
		keyA[4] = (byte) 0x90;
		keyA[5] = (byte) 0x69;
		return keyA;
	}

}
