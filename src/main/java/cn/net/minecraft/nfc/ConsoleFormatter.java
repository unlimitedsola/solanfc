package cn.net.minecraft.nfc;

import java.io.File;

/**
 * ***********************************************
 * Created by Sola on 2014/6/28.
 * Don't modify this source without my agreement
 * ***********************************************
 */
public class ConsoleFormatter {

	public static void printProtocolDebugInfo(int SW) {
		StackTraceElement trace = new Throwable().getStackTrace()[1];
		System.err.printf("=====Start Protocol Debug Info=====\n" +
						"Class: %s\n" +
						"Failed while processing: %s\n" +
						"Response Code: %x\n" +
						"=====End Protocol Debug Info=====\n",
				trace.getClassName(), trace.getMethodName(), SW);
	}

	public static void printIODebugInfo(String messages, File file, int lineNumber, String txt) {
		StackTraceElement trace = new Throwable().getStackTrace()[1];
		System.err.printf("=====Start IO Debug Info=====\n" +
						"Class: %s\n" +
						"Failed while processing: %s\n" +
						"Info: %s\n" +
						"File: %s\n" +
						"Line: %d\n" +
						"Text: \"%s...\"\n" +
						"=====End IO Debug Info=====\n",
				trace.getClassName(), trace.getMethodName(), messages, file, lineNumber, txt.substring(0, Math.min(txt.length(), 20)));

	}

}
