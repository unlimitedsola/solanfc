package cn.net.minecraft.nfc.data;

import cn.net.minecraft.nfc.util.ByteArrayUtil;

import javax.smartcardio.ATR;

/**
 * ***********************************************
 * Created by Sola on 2014/6/16.
 * Don't modify this source without my agreement
 * ***********************************************
 */
public enum MifareClassicType {

	MIFARE_CLASSIC_1K, MIFARE_CLASSIC_4K;

	private static final byte[] ATR_HEAD = {(byte) 0x3B, (byte) 0x8F, (byte) 0x80, (byte) 0x01, (byte) 0x80, (byte) 0x4F, (byte) 0x0C};
	private static final byte ATR_STANDARD = 0x03;

	public static MifareClassicType getByATR(ATR atr) {
		byte[] b = atr.getBytes();
		//3B 8F 80 01 80 4F 0C
		if (!ByteArrayUtil.arrayEqual(b, 0, ATR_HEAD, 0, ATR_HEAD.length)) return null;
		if (b[12] != ATR_STANDARD) return null;
		if (b[13] == 0 && b[14] == 1) {
			return MifareClassicType.MIFARE_CLASSIC_1K;
		}
		if (b[13] == 0 && b[14] == 2) {
			return MifareClassicType.MIFARE_CLASSIC_4K;
		}
		return null;
	}

}
