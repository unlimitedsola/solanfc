package cn.net.minecraft.nfc.util;

import cn.net.minecraft.nfc.data.Key;
import cn.net.minecraft.nfc.protocol.MifareClassic;
import org.apache.commons.lang3.Validate;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

/**
 * ***********************************************
 * Created by Sola on 2014/10/12.
 * Don't modify this source without my agreement
 * ***********************************************
 */
public class mfocConverter {

	public static String keyPath = "H:\\mfocGUI_v29\\compiled\\Keys\\";

	public static Key[] convertKeyFile(String uid, int keyType) {
		Validate.isTrue(keyType == MifareClassic.KEY_A || keyType == MifareClassic.KEY_B, "KeyType must be 0x60(A) or 0x61(B)");
		char type = keyType == MifareClassic.KEY_A ? 'a' : 'b';
		File file = new File(keyPath, type + uid + ".dump");
		if (!file.exists()) {
			return null;
		}
		int length = (int) file.length();
		Key[] keys = new Key[length / 6];
		try {
			FileInputStream fin = new FileInputStream(file);
			byte[] buf = new byte[6];
			for (int i = 0; i < keys.length; i++) {
				fin.read(buf);
				keys[i] = new Key(buf);
				Arrays.fill(buf, (byte) -1);
			}
		} catch (FileNotFoundException e) {
			return null;
		} catch (IOException e) {
			return null;
		}
		return keys;
	}

}
