package cn.net.minecraft.nfc.test;

import cn.net.minecraft.nfc.protocol.MifareClassic;
import cn.net.minecraft.nfc.util.mfocConverter;

import java.util.Arrays;

/**
 * ***********************************************
 * Created by Sola on 2014/10/11.
 * Don't modify this source without my agreement
 * ***********************************************
 */
public class RAW2MCTDump {

	public static void main(String[] args) {
//		try {
//			FileInputStream fin = new FileInputStream("a1b9af795.dump");
//			byte[] buf = new byte[16];
//			for (int i = 0; i < 256; i++) {
//				fin.read(buf);
//				System.out.println(ByteArrayUtil.bytesToHex(buf));
//			}
//			fin.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		System.out.println(Arrays.toString(mfocConverter.convertKeyFile("1b9af795", MifareClassic.KEY_A)));
	}

}
