package cn.net.minecraft.nfc.calculator;

import static cn.net.minecraft.nfc.util.ByteArrayUtil.hexToBytes;

/**
 * ***********************************************
 * Created by Sola on 2014/6/30.
 * Don't modify this source without my agreement
 * ***********************************************
 */
@SuppressWarnings("PointlessBitwiseExpression")
public class WaterCalculator {

	public static byte[] calculateKey(byte[] uid) {
		final byte[] KEYGEN = hexToBytes("91364369");
		byte[] keyA = new byte[6];
		for (int j = 0; j < 4; j++) {
			keyA[j] = (byte) (uid[j] ^ KEYGEN[j]);
		}
		keyA[4] = 0x33;
		keyA[5] = (byte) 0x90;
		return keyA;
	}

	public static int calculateBalance(byte[] data) {
		int data1 = data[4] & 0xFF;
		int data2 = data[5] & 0xFF;
		return (data1 << 8) | data2;
	}

	public static byte calculateCheckBit(int balance) {
		byte data1 = (byte) ((balance >>> 8) & 0xFF);
		byte data2 = (byte) ((balance >>> 0) & 0xFF);
		return (byte) ((data1 ^ 0x64) ^ data2);
	}

	public static boolean checkBit(byte[] data) {
		return calculateCheckBit(calculateBalance(data)) == data[6];
	}

	public static void applyNewBalance(byte[] data, int balance) {
		byte data1 = (byte) ((balance >>> 8) & 0xFF);
		byte data2 = (byte) ((balance >>> 0) & 0xFF);
		data[4] = data1;
		data[5] = data2;
		data[6] = calculateCheckBit(balance);
	}

}
