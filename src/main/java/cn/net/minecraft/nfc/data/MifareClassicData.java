package cn.net.minecraft.nfc.data;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

import java.util.Arrays;

/**
 * ***********************************************
 * Created by Sola on 2014/6/16.
 * Don't modify this source without my agreement
 * ***********************************************
 */
@EqualsAndHashCode
@ToString
public class MifareClassicData {

	private final Sector[] sectors;
	@Getter
	private final MifareClassicType type;

	public MifareClassicData(MifareClassicType type) {
		Validate.notNull(type);
		switch (type) {
			case MIFARE_CLASSIC_1K:
				sectors = new Sector[16];
				break;
			case MIFARE_CLASSIC_4K:
				sectors = new Sector[40];
			default:
				throw new IllegalArgumentException("Invalid Card Type");
		}
		this.type = type;
	}

	public Sector[] getSectors() {
		return Arrays.copyOf(sectors, sectors.length);
	}

	public void autoInit() {

	}


}
