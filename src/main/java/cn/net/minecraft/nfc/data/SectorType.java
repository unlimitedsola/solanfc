package cn.net.minecraft.nfc.data;

/**
 * ***********************************************
 * Created by Sola on 2014/6/16.
 * Don't modify this source without my agreement
 * ***********************************************
 */
@Deprecated
public enum SectorType {

	GENERIC,
	EXTENDED;

}
