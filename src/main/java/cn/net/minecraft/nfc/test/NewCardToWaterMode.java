package cn.net.minecraft.nfc.test;

import cn.net.minecraft.nfc.calculator.WaterCalculator;
import cn.net.minecraft.nfc.data.Block;
import cn.net.minecraft.nfc.data.Key;
import cn.net.minecraft.nfc.data.MifareClassicType;
import cn.net.minecraft.nfc.device.ACR122;
import cn.net.minecraft.nfc.protocol.MifareClassic;

import javax.smartcardio.Card;
import javax.smartcardio.CardException;

import static cn.net.minecraft.nfc.util.ByteArrayUtil.bytesToHex;

/**
 * ***********************************************
 * Created by Sola on 2014/10/9.
 * Don't modify this source without my agreement
 * ***********************************************
 */
public class NewCardToWaterMode {

	public static void main(String[] args) throws Exception {
		int value;
		if (args.length == 0) {
			value = 1000;
		} else {
			value = Integer.valueOf(args[0]);
		}
		ACR122.initTerminal();
		while (true) {
			try {
				Card card = ACR122.getAvailableCard();
				System.out.println("=====New Card Detected=====");
				MifareClassic m1 = new MifareClassic(card);
				if (m1.getType() != MifareClassicType.MIFARE_CLASSIC_1K) {
					System.err.println("Error: Invalided Card!");
					sendErrorLED(m1);
					endOperation();
					continue;
				}
				byte[] UID = m1.getUID();
				System.out.println("Card UID: " + bytesToHex(UID));
				Key calculatedKeyA = new Key(WaterCalculator.calculateKey(UID));
				Key keyA = Key.genDefault();
				System.out.println("Calculated KeyA: " + calculatedKeyA);
				if (!m1.authBlock(keyA, 48, MifareClassic.KEY_A)) {
					System.err.println("Error: Invalided Card!");
					sendErrorLED(m1);
					endOperation();
					continue;
				}
				Block data = new Block("01204500000064F1FFFFFFFFFFFFFFFF");
				Block keyBlock = new Block(calculatedKeyA.hexString() + "FF078069FFFFFFFFFFFF");
				if (!WaterCalculator.checkBit(data.getData())) {
					System.err.println("Error: Invalided Card!");
					sendErrorLED(m1);
					endOperation();
					continue;
				}
				System.out.print("Writing Data : ");
				if (!m1.writeBlock(48, data) || !m1.writeBlock(51, keyBlock)) {
					System.out.println("Failed!");
					sendErrorLED(m1);
					endOperation();
					continue;
				}
				System.out.println("Done!");
				sendBuzzer(m1);
				endOperation();
			} catch (Exception e) {
				System.err.println("Error: Internal Error!");
				endOperation();
			}
		}
	}

	public static void sendBuzzer(MifareClassic m1) {
		m1.controlLED(0b00100000, 1, 2, 2, 1);
	}

	public static void sendErrorLED(MifareClassic m1) {
		m1.controlLED(0b01100000, 1, 1, 10, 1);
	}

	public static void endOperation() throws CardException {
		System.out.println("Waiting for removing the card...");
		ACR122.waitForAbsent(0);
		System.out.println("=====Card Operation End=====");
	}

}
