package cn.net.minecraft.nfc.test;

import cn.net.minecraft.nfc.calculator.FoodCalculator;
import cn.net.minecraft.nfc.data.Block;
import cn.net.minecraft.nfc.data.Key;
import cn.net.minecraft.nfc.data.MifareClassicType;
import cn.net.minecraft.nfc.device.ACR122;
import cn.net.minecraft.nfc.protocol.MifareClassic;

import javax.smartcardio.Card;
import javax.smartcardio.CardException;

import static cn.net.minecraft.nfc.util.ByteArrayUtil.bytesToHex;

/**
 * ***********************************************
 * Created by Sola on 2014/7/1.
 * Don't modify this source without my agreement
 * ***********************************************
 */
public class FoodMode {

	public static void main(String[] args) throws Exception {
		ACR122.initTerminal();
		while (true) {
			try {
				Card card = ACR122.getAvailableCard();
				System.out.println("=====New Card Detected=====");
				MifareClassic m1 = new MifareClassic(card);
				if (m1.getType() != MifareClassicType.MIFARE_CLASSIC_1K) {
					System.err.println("Error: Invalided Card!");
					sendErrorLED(m1);
					endOperation();
					continue;
				}
				byte[] UID = m1.getUID();
				System.out.println("Card UID: " + bytesToHex(UID));
				Key keyA = new Key(FoodCalculator.calculateKey(UID));
				System.out.println("Calculated KeyA: " + bytesToHex(keyA.getData()));
				if (!m1.authBlock(keyA, 0x34, MifareClassic.KEY_A)) {
					System.err.println("Error: Invalided Card!");
					sendErrorLED(m1);
					endOperation();
					continue;
				}
				for (int i = 0x34; i < 0x37; i++) {
					System.out.println("Block " + i + ": " + bytesToHex(m1.readBlock(i).getData()));
				}
				Block data1 = new Block("000200B90100000000000000000000BC");
				Block data2 = new Block("001201001313BA02032104000022003F");
				System.out.print("Writing Data : ");
				boolean flag = m1.writeBlock(0x34, data1) && m1.writeBlock(0x35, data2) && m1.writeBlock(0x36, data2);
				if (!flag) {
					System.out.println("Failed!");
					sendErrorLED(m1);
					endOperation();
					continue;
				}
				System.out.println("Done!");
				sendBuzzer(m1);
				endOperation();
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("Error: Internal Error!");
				endOperation();
			}
		}
	}

	public static void sendBuzzer(MifareClassic m1) {
		m1.controlLED(0b00100000, 1, 2, 2, 1);
	}

	public static void sendErrorLED(MifareClassic m1) {
		m1.controlLED(0b01100000, 1, 1, 10, 1);
	}

	public static void endOperation() throws CardException {
		System.out.println("Waiting for removing the card...");
		ACR122.waitForAbsent(0);
		System.out.println("=====Card Operation End=====");
	}
}
