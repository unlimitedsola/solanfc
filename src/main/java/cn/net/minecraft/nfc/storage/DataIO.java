package cn.net.minecraft.nfc.storage;

import cn.net.minecraft.nfc.ConsoleFormatter;
import cn.net.minecraft.nfc.SolaNFC;
import cn.net.minecraft.nfc.data.Key;
import cn.net.minecraft.nfc.util.ByteArrayUtil;
import org.apache.commons.lang3.Validate;

import java.io.File;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.util.LinkedList;
import java.util.List;

import static cn.net.minecraft.nfc.util.ByteArrayUtil.bytesToHex;

/**
 * ***********************************************
 * Created by Sola on 2014/6/21.
 * Don't modify this source without my agreement
 * ***********************************************
 */
public class DataIO {

	public static File Key_File_Path = new File("data/keys");
	public static File Dump_File_Path = new File("data/dumps");

	public static File getStoredKeyFile(String name) {
		if (!Key_File_Path.exists()) {
			try {
				Validate.isTrue(Key_File_Path.mkdirs());
			} catch (Exception e) {
				System.err.println("Failed to create key dir: " + e.getMessage());
			}
		}
		return new File(Key_File_Path, name);
	}

	public static File getStoredDumpFile(String name) {
		if (!Dump_File_Path.exists()) {
			try {
				Validate.isTrue(Dump_File_Path.mkdirs());
			} catch (Exception e) {
				System.err.println("Failed to create dump dir: " + e.getMessage());
			}
		}
		return new File(Dump_File_Path, name);
	}

	public static List<Key> byStored(String filename) {
		File file = getStoredKeyFile(filename);
		List<Key> keys = new LinkedList<>();
		if (!file.exists()) return null;
		try {
			LineNumberReader reader = new LineNumberReader(new FileReader(file));
			String line;
			while ((line = reader.readLine()) != null) {
				if (line.startsWith("#")) continue;
				if (line.length() != 12) {
					if (SolaNFC.DEBUG_MODE)
						ConsoleFormatter.printIODebugInfo("Invalided Key", file, reader.getLineNumber(), line);
					continue;
				}
				byte[] key = ByteArrayUtil.hexToBytes(line);

				System.out.println("Loading Key: "+bytesToHex(key));
				keys.add(new Key(key));
			}
			reader.close();
			return keys;
		} catch (Exception e) {
			System.err.println("An error occurred while reading key file: " + e.getMessage());
			return keys;
		}
	}

}
