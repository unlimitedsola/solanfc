package cn.net.minecraft.nfc.test;

import cn.net.minecraft.nfc.data.Block;
import cn.net.minecraft.nfc.data.Key;
import cn.net.minecraft.nfc.data.MifareClassicType;
import cn.net.minecraft.nfc.device.ACR122;
import cn.net.minecraft.nfc.protocol.MifareClassic;
import org.apache.commons.lang3.ArrayUtils;

import javax.smartcardio.Card;
import javax.smartcardio.CardException;
import java.nio.charset.StandardCharsets;

import static cn.net.minecraft.nfc.util.ByteArrayUtil.bytesToHex;

/**
 * ***********************************************
 * Created by Sola on 2014/10/9.
 * Don't modify this source without my agreement
 * ***********************************************
 */
public class NewCardToDJMAXMode {

//	public static final String serial = "1UZVVIXZOJFL2U12FEDY";
	public static final String serial = "YHRKTKEYOD8313P6RJE5";

	public static final Key keyA = new Key("3721536A7240");
	public static final Block keyBlock = new Block("3721536A7240FF078069FFFFFFFFFFFF");

	public static void main(String[] args) throws Exception {
		ACR122.initTerminal();
		root:
		while (true) {
			try {
				Card card = ACR122.getAvailableCard();
				System.out.println("=====New Card Detected=====");
				MifareClassic m1 = new MifareClassic(card);
				if (m1.getType() != MifareClassicType.MIFARE_CLASSIC_1K) {
					System.err.println("Error: Invalided Card!");
					sendErrorLED(m1);
					continue;
				}
				byte[] UID = m1.getUID();
				System.out.println("Card UID: " + bytesToHex(UID));
				Key keyA = Key.genDefault();
				if (!m1.authBlock(keyA, 0, MifareClassic.KEY_A)) {
					System.err.println("Error: Invalided Card!");
					sendErrorLED(m1);
					continue;
				}
				Block data1 = new Block(serial.substring(0, 16).getBytes(StandardCharsets.US_ASCII));
				Block data2 = new Block(ArrayUtils.addAll(serial.substring(16).getBytes(StandardCharsets.US_ASCII), new byte[12]));
				System.out.print("Writing Data : ");
				if (!m1.writeBlock(1, data1) || !m1.writeBlock(2, data2)) {
					System.out.println("Failed!");
					sendErrorLED(m1);
					continue;
				}
				System.out.println("Done!");
				System.out.print("Writing Key : ");
				for (int i = 0; i < 16; i++) {
					if (!m1.authBlock(keyA, i * 4 + 3, MifareClassic.KEY_A) || !m1.writeBlock(i * 4 + 3, keyBlock)) {
						System.out.println("Failed!");
						sendErrorLED(m1);
						continue root;
					}
				}
				System.out.println("Done!");
				sendBuzzer(m1);
			} catch (Exception e) {
				System.err.println("Error: Internal Error!");
			} finally {
				endOperation();
			}
		}
	}

	public static void sendBuzzer(MifareClassic m1) {
		m1.controlLED(0b00100000, 1, 2, 2, 1);
	}

	public static void sendErrorLED(MifareClassic m1) {
		m1.controlLED(0b01100000, 1, 1, 10, 1);
	}

	public static void endOperation() throws CardException {
		System.out.println("Waiting for removing the card...");
		ACR122.waitForAbsent(0);
		System.out.println("=====Card Operation End=====");
	}

}
