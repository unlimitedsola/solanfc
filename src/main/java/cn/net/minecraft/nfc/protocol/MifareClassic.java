package cn.net.minecraft.nfc.protocol;

import cn.net.minecraft.nfc.ConsoleFormatter;
import cn.net.minecraft.nfc.SolaNFC;
import cn.net.minecraft.nfc.data.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.apache.commons.lang3.Validate;

import javax.smartcardio.*;

import static cn.net.minecraft.nfc.util.ByteArrayUtil.bytesToHex;
import static cn.net.minecraft.nfc.util.ByteArrayUtil.hexToBytes;

/**
 * ***********************************************
 * Created by Sola on 2014/6/18.
 * Don't modify this source without my agreement
 * ***********************************************
 */
@EqualsAndHashCode
public class MifareClassic {

	public static final byte KEY_A = 0x60;
	public static final byte KEY_B = 0x61;

	@Getter
	Card handle;
	@Getter
	MifareClassicType type;

	public MifareClassic(Card handle) {
		this.handle = handle;
		type = MifareClassicType.getByATR(handle.getATR());
		Validate.notNull(type, "Invalid Card");
	}

	public byte[] getUID() {
		try {
			CommandAPDU cmd = new CommandAPDU(new byte[]{(byte) 0xFF, (byte) 0xCA, 0x00, 0x00, 0x00});
			ResponseAPDU response = transmit(cmd);
			if (response.getSW() == 0x9000) {
				return response.getData();
			}
			if (SolaNFC.DEBUG_MODE)
				ConsoleFormatter.printProtocolDebugInfo(response.getSW());
		} catch (CardException e) {
			System.err.println("An error occurred while getting card UID: " + e.getMessage());
			return null;
		}
		return null;
	}

	public Block readBlock(int index) {
		Validate.isTrue(checkIndex(index), "Invalid Block Index");
		try {
			CommandAPDU cmd = new CommandAPDU(0xFF, 0xB0, 0x00, index, 16);
			ResponseAPDU response = transmit(cmd);
			if (response.getSW() == 0x9000) {
				return new Block(response.getData());
			}
			if (SolaNFC.DEBUG_MODE)
				ConsoleFormatter.printProtocolDebugInfo(response.getSW());
		} catch (CardException e) {
			System.err.println("An error occurred while reading block: " + e.getMessage());
			return null;
		}
		return null;
	}

	public boolean writeBlock(int index, Block block) {
		Validate.isTrue(checkIndex(index), "Invalid Block Index");
		try {
			CommandAPDU cmd = new CommandAPDU(0xFF, 0xD6, 0x00, index, block.getData());
			ResponseAPDU response = transmit(cmd);
			if (response.getSW() == 0x9000) {
				return true;
			}
			if (SolaNFC.DEBUG_MODE)
				ConsoleFormatter.printProtocolDebugInfo(response.getSW());
		} catch (CardException e) {
			System.err.println("An error occurred while writing block: " + e.getMessage());
			return false;
		}
		return false;
	}

	public boolean authBlock(Key key, int block, int keyType) {
		return loadKey(key) && authKey(block, keyType);
	}

	public boolean loadKey(Key key) {
		try {
			CommandAPDU cmd = new CommandAPDU(0xFF, 0x82, 0x00, 0x00, key.getData());
			ResponseAPDU response = transmit(cmd);
			if (response.getSW() == 0x9000) {
				return true;
			}
			if (SolaNFC.DEBUG_MODE)
				ConsoleFormatter.printProtocolDebugInfo(response.getSW());
		} catch (CardException e) {
			System.err.println("An error occurred while loading key: " + e.getMessage());
			return false;
		}
		return false;
	}

	public boolean authKey(int block, int keyType) {
		Validate.isTrue(checkIndex(block), "Invalid Block Index");
		Validate.isTrue(keyType == KEY_A || keyType == KEY_B, "KeyType must be 0x60(A) or 0x61(B)");
		try {
			CommandAPDU cmd = new CommandAPDU(0xFF, 0x86, 0x00, 0x00, new byte[]{0x01, 0x00, (byte) block, (byte) keyType, 0x00});
			ResponseAPDU response = transmit(cmd);
			if (response.getSW() == 0x9000) {
				return true;
			}
			if (SolaNFC.DEBUG_MODE)
				ConsoleFormatter.printProtocolDebugInfo(response.getSW());
		} catch (CardException e) {
			System.err.println("An error occurred while authenticating key: " + e.getMessage());
			return false;
		}
		return false;
	}

	public void controlLED(int P2, int T1, int T2, int count, int buzzer) {
		try {
			CommandAPDU cmd = new CommandAPDU(new byte[]{(byte) 0xFF, 0x00, 0x40, (byte) P2, 0x04, (byte) T1, (byte) T2, (byte) count, (byte) buzzer});
			transmit(cmd);
		} catch (CardException e) {
			System.err.println("An error occurred while controlling LED: " + e.getMessage());
		}
	}

	public void modifyUID(Block uid) {
		try {
			byte[] data = uid.getData();
			data[4] = (byte) (data[0] ^ data[1] ^ data[2] ^ data[3]);
			//System.out.println(bytesToHex(data));

			transmitRaw("FF00000008D408630200630300");
			transmitRaw("FF00000006D442500057CD");
			transmitRaw("FF00000005D408633D07");
			transmitRaw("FF00000003D44240");
			transmitRaw("FF00000005D408633D00");
			transmitRaw("FF00000003D44243");
			transmitRaw("FF00000008D408630280630380");
			transmitRaw("FF00000015D44001A000" + bytesToHex(data));
		} catch (CardException e) {
			System.err.println("An error occurred while setting UID: " + e.getMessage());
		}
	}

	public byte[] transmitRaw(byte[] data) throws CardException {
		CardChannel ch = handle.getBasicChannel();
		CommandAPDU cmd = new CommandAPDU(data);
		ResponseAPDU response = ch.transmit(cmd);
		return response.getBytes();
	}

	public byte[] transmitRaw(String data) throws CardException {
		CardChannel ch = handle.getBasicChannel();
		CommandAPDU cmd = new CommandAPDU(hexToBytes(data));
		ResponseAPDU response = ch.transmit(cmd);
		return response.getBytes();
	}

	// -------------------------------------------- //
	// MULTI PLATFORM
	// -------------------------------------------- //
	public ResponseAPDU transmit(CommandAPDU cmd) throws CardException {
		CardChannel ch = handle.getBasicChannel();
		return ch.transmit(cmd);
	}

	private boolean checkIndex(int block) {
		if (type == MifareClassicType.MIFARE_CLASSIC_1K) {
			return block >= 0 && block <= 0x3F;
		}else if (type == MifareClassicType.MIFARE_CLASSIC_4K) {
			return block >= 0 && block <= 0xFF;
		}
		return false;
	}

}
