package cn.net.minecraft.nfc.test;

import cn.net.minecraft.nfc.calculator.WaterCalculator;
import cn.net.minecraft.nfc.data.Block;
import cn.net.minecraft.nfc.data.Key;
import cn.net.minecraft.nfc.data.MifareClassicType;
import cn.net.minecraft.nfc.device.ACR122;
import cn.net.minecraft.nfc.protocol.MifareClassic;

import javax.smartcardio.Card;
import javax.smartcardio.CardException;

import static cn.net.minecraft.nfc.util.ByteArrayUtil.bytesToHex;

/**
 * ***********************************************
 * Created by Sola on 2014/6/30.
 * Don't modify this source without my agreement
 * ***********************************************
 */
public class WaterMode {

	@SuppressWarnings("SpellCheckingInspection")
	public static void main(String[] args) throws Exception {
		int value;
		if (args.length == 0) {
			value = 6000;
		} else {
			value = Integer.valueOf(args[0]);
		}
		ACR122.initTerminal();
		while (true) {
			try {
				Card card = ACR122.getAvailableCard();
				System.out.println("=====New Card Detected=====");
				MifareClassic m1 = new MifareClassic(card);
				if (m1.getType() != MifareClassicType.MIFARE_CLASSIC_1K) {
					System.err.println("Error: Invalided Card!");
					sendErrorLED(m1);
					endOperation();
					continue;
				}
				byte[] UID = m1.getUID();
				System.out.println("Card UID: " + bytesToHex(UID));
				Key keyA = new Key(WaterCalculator.calculateKey(UID));
				System.out.println("Calculated KeyA: " + keyA);
				if (!m1.authBlock(keyA, 48, MifareClassic.KEY_A)) {
					System.err.println("Error: Invalided Card!");
					sendErrorLED(m1);
					endOperation();
					continue;
				}
//				Block block = m1.readBlock(48);
				Block block = new Block("012045000BB8D7F1FFFFFFFFFFFFFFFF");
				byte[] data = block.getData();
				System.out.println("Read Block 48 Data: " + block);
				if (!WaterCalculator.checkBit(data)) {
					System.err.println("Error: Invalided Card!");
					sendErrorLED(m1);
					endOperation();
					continue;
				}
				int balance = WaterCalculator.calculateBalance(data);
				System.out.printf("Current Balance: %.2f\n", balance / 100D);
//				balance = Math.max(Math.min(0xFFFF, balance + value), 0);
//				balance += 10000;
				balance = value;
				System.out.printf("New Balance: %.2f\n", balance / 100D);
				WaterCalculator.applyNewBalance(data, balance);
				System.out.println("New Data: " + block);
				System.out.print("Writing Data : ");
				if (!m1.writeBlock(48, new Block(data))) {
					System.out.println("Failed!");
					sendErrorLED(m1);
					endOperation();
					continue;
				}
				System.out.println("Done!");
				sendBuzzer(m1);
				endOperation();
			} catch (Exception e) {
				System.err.println("Error: Internal Error!");
				endOperation();
			}
		}
	}

	public static void sendBuzzer(MifareClassic m1) {
		m1.controlLED(0b00100000, 1, 2, 2, 1);
	}

	public static void sendErrorLED(MifareClassic m1) {
		m1.controlLED(0b01100000, 1, 1, 10, 1);
	}

	public static void endOperation() throws CardException {
		System.out.println("Waiting for removing the card...");
		ACR122.waitForAbsent(0);
		System.out.println("=====Card Operation End=====");
	}

}
