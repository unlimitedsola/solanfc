package cn.net.minecraft.nfc;

import joptsimple.OptionParser;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static java.util.Arrays.asList;

/**
 * ***********************************************
 * Created by Sola on 2014/6/14.
 * Don't modify this source without my agreement
 * ***********************************************
 */
public class SolaNFC {

	public static boolean DEBUG_MODE = false;

	public static void main(String[] args) {
		OptionParser parser = new OptionParser() {
			{
				acceptsAll(asList("?", "help"), "Show the help");

				acceptsAll(asList("a", "first-jar"), "First jar with original names, for generating mapping")
						.withRequiredArg()
						.ofType(String.class);

				acceptsAll(asList("b", "second-jar"), "Second jar with renamed names, for generating mapping")
						.withRequiredArg()
						.ofType(String.class);

				acceptsAll(asList("s", "srg-out"), "Mapping file output")
						.withRequiredArg()
						.ofType(File.class);

				acceptsAll(asList("c", "compact"), "Output mapping file in compact format");
				acceptsAll(asList("f", "generate-dupes"), "Include unrenamed symbols in mapping file output");

				acceptsAll(asList("m", "srg-in"), "Mapping file input")
						.withRequiredArg()
						.ofType(String.class);

				acceptsAll(asList("n", "numeric-srg"), "Use numeric .srg mappings with srg-in dir (num->mcp vs obf->mcp)");

				acceptsAll(asList("R", "in-shade-relocation", "shade-relocation"), "Simulate maven-shade-plugin relocation patterns on srg-in input names")
						.withRequiredArg();

				acceptsAll(asList("out-shade-relocation"), "Simulate maven-shade-plugin relocation patterns on srg-in output names")
						.withRequiredArg();

				acceptsAll(asList("r", "reverse"), "Reverse input/output names on srg-in");

				acceptsAll(asList("i", "in-jar"), "Input jar(s) to remap")
						.withRequiredArg()
						.ofType(String.class);

				acceptsAll(asList("o", "out-jar"), "Output jar to write")
						.withRequiredArg()
						.ofType(File.class);


				acceptsAll(asList("force-redownload"), "Force redownloading remote resources (invalid cache)");

				acceptsAll(asList("l", "live"), "Enable runtime inheritance lookup");
				acceptsAll(asList("L", "live-remapped"), "Enable runtime inheritance lookup through a mapping");

				acceptsAll(asList("H", "write-inheritance"), "Write inheritance map to file")
						.withRequiredArg()
						.ofType(File.class);
				acceptsAll(asList("h", "read-inheritance"), "Read inheritance map from file")
						.withRequiredArg()
						.ofType(String.class);

				//acceptsAll(asList("G", "remap-reflect-field"), "Remap reflection calls to getDeclaredField()"); // TODO

				acceptsAll(asList("q", "quiet"), "Quiet mode");

				acceptsAll(asList("v", "version"), "Displays version information");

				acceptsAll(asList("kill-source"), "Removes the \"SourceFile\" attribute");
				acceptsAll(asList("kill-lvt"), "Removes the \"LocalVariableTable\" attribute");
				acceptsAll(asList("kill-generics"), "Removes the \"LocalVariableTypeTable\" and \"Signature\" attributes");
				acceptsAll(asList("d", "identifier"), "Identifier to place on each class that is transformed, by default, none")
						.withRequiredArg()
						.ofType(String.class);
				acceptsAll(asList("e", "excluded-packages"), "A comma separated list of packages that should not be transformed, even if the srg specifies they should")
						.withRequiredArg()
						.ofType(String.class);
			}
		};
		try {
			parser.printHelpOn(System.out);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test() {
		main(null);
	}

}
