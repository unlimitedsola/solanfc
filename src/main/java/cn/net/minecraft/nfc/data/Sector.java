package cn.net.minecraft.nfc.data;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.apache.commons.lang3.Validate;

import java.util.Arrays;

/**
 * ***********************************************
 * Created by Sola on 2014/6/16.
 * Don't modify this source without my agreement
 * ***********************************************
 */
@Deprecated
@EqualsAndHashCode
public class Sector {

	private final Block[] blocks;
	@Getter
	private final SectorType type;

	public Sector(Block[] blocks) {
		Validate.notNull(blocks);
		Validate.isTrue(blocks.length == 4 || blocks.length == 16);
		if (blocks.length == 16) {
			type = SectorType.EXTENDED;
		} else {
			type = SectorType.GENERIC;
		}
		this.blocks = blocks;
	}

	public boolean isControlBlock(int index) {
		return index == blocks.length - 1;
	}

	public Block[] getBlocks() {
		return Arrays.copyOf(blocks, blocks.length);
	}

	@Override
	public String toString() {
		return "Sector{" +
				"blocks=" + Arrays.toString(blocks) +
				", type=" + type +
				'}';
	}
}
