package cn.net.minecraft.nfc.data;

import cn.net.minecraft.nfc.util.ByteArrayUtil;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.Validate;

import java.io.Serializable;
import java.util.Arrays;

import static cn.net.minecraft.nfc.util.ByteArrayUtil.bytesToHex;

/**
 * ***********************************************
 * Created by Sola on 2014/6/16.
 * Don't modify this source without my agreement
 * ***********************************************
 */
@EqualsAndHashCode
public class Block implements Cloneable, Serializable {

	private final byte[] data;

	public Block() {
		this(new byte[16]);
	}

	public Block(byte[] data) {
		Validate.isTrue(data.length == 16, "Block must contains 16 bytes");
		this.data = data;
	}

	public Block(String hex) {
		this(ByteArrayUtil.hexToBytes(hex));
	}

	public static Block genDefault() {
		return new Block(ByteArrayUtil.hexToBytes("00000000000000000000000000000000"));
	}

	public static boolean isControlBlock(int index) {
		if (index > 0x7F) {
			return ++index % 16 == 0;
		} else {
			return ++index % 4 == 0;
		}
	}

	public static int getSectorIndex(int blockIndex) {
		if (blockIndex > 0x7F) {
			return (blockIndex - 0x80) / 16 + 32;
		} else {
			return blockIndex / 4;
		}
	}

	public byte[] getData() {
		return Arrays.copyOf(data, data.length);
	}

	@Override
	public String toString() {
		return "Block{" + bytesToHex(data) + "}";
	}

}
