package cn.net.minecraft.nfc.data;

import cn.net.minecraft.nfc.util.ByteArrayUtil;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.Validate;

import java.io.Serializable;
import java.util.Arrays;

import static cn.net.minecraft.nfc.util.ByteArrayUtil.bytesToHex;

/**
 * ***********************************************
 * Created by Sola on 2014/6/21.
 * Don't modify this source without my agreement
 * ***********************************************
 */
@EqualsAndHashCode
public class Key implements Cloneable, Serializable {

	private final byte[] data;

	public Key() {
		data = new byte[6];
	}

	public Key(byte[] data) {
		Validate.isTrue(data != null && data.length == 6, "Key data must contain 6 bytes.");
		this.data = Arrays.copyOf(data, data.length);
	}

	public Key(String hex) {
		this(ByteArrayUtil.hexToBytes(hex));
	}

	public String hexString() {
		return bytesToHex(data);
	}

	public static Key genDefault() {
		return new Key(ByteArrayUtil.hexToBytes("FFFFFFFFFFFF"));
	}

	public byte[] getData() {
		return Arrays.copyOf(data, data.length);
	}

	@Override
	public String toString() {
		return "Key{" + bytesToHex(data) + "}";
	}

}
