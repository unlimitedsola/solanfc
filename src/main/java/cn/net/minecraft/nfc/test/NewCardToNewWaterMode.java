package cn.net.minecraft.nfc.test;

import cn.net.minecraft.nfc.data.Block;
import cn.net.minecraft.nfc.data.Key;
import cn.net.minecraft.nfc.data.MifareClassicType;
import cn.net.minecraft.nfc.device.ACR122;
import cn.net.minecraft.nfc.protocol.MifareClassic;

import javax.smartcardio.Card;
import javax.smartcardio.CardException;

/**
 * ***********************************************
 * Created by Sola on 2014/10/9.
 * Don't modify this source without my agreement
 * ***********************************************
 */
public class NewCardToNewWaterMode {

	public static void main(String[] args) throws Exception {
		int value;
		if (args.length == 0) {
			value = 1000;
		} else {
			value = Integer.valueOf(args[0]);
		}
		ACR122.initTerminal();
		while (true) {
			try {
				Card card = ACR122.getAvailableCard();
				System.out.println("=====New Card Detected=====");
				MifareClassic m1 = new MifareClassic(card);
				if (m1.getType() != MifareClassicType.MIFARE_CLASSIC_1K) {
					System.err.println("Error: Invalided Card!");
					sendErrorLED(m1);
					endOperation();
					continue;
				}
				m1.modifyUID(new Block("9B408F75210804006263646566676869"));

				card.disconnect(true);
				Thread.sleep(2000);
				card = ACR122.getAvailableCard();
				m1 = new MifareClassic(card);

//				Key keyA = Key.genDefault();
				Key keyA = new Key("9638FF4CF2A5");
				if (!m1.authBlock(keyA, 0xf, MifareClassic.KEY_A)) {
					System.err.println("Error: Invalided Card!");
					sendErrorLED(m1);
					endOperation();
					continue;
				}
				Block data1 = new Block("000E0001000000303030303030313494");
//				Block data2 = new Block("FFFF000000000F020413000101000028");
				Block data2 = new Block("9F86010000000F020413000101000050");
				Block keyBlock = new Block("9638FF4CF2A5FF078069FB00F1D85EA5");
				System.out.print("Writing Data : ");
				if (!m1.authBlock(keyA, 0xF, MifareClassic.KEY_A)
						|| !m1.writeBlock(0xc, data1)
						|| !m1.writeBlock(0xd, data2)
						|| !m1.writeBlock(0xe, data2)
						|| !m1.writeBlock(0xf, keyBlock)
						|| !m1.authBlock(keyA, 0x13, MifareClassic.KEY_A)
						|| !m1.writeBlock(0x13, keyBlock)) {
					System.out.println("Failed!");
					sendErrorLED(m1);
					endOperation();
					continue;
				}
				System.out.println("Done!");
				sendBuzzer(m1);
				endOperation();
			} catch (Exception e) {
				System.err.println("Error: Internal Error!");
				endOperation();
			}
		}
	}

	public static void sendBuzzer(MifareClassic m1) {
		m1.controlLED(0b00100000, 1, 2, 2, 1);
	}

	public static void sendErrorLED(MifareClassic m1) {
		m1.controlLED(0b01100000, 1, 1, 10, 1);
	}

	public static void endOperation() throws CardException {
		System.out.println("Waiting for removing the card...");
		ACR122.waitForAbsent(0);
		System.out.println("=====Card Operation End=====");
	}

}
