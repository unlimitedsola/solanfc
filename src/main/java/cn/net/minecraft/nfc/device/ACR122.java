package cn.net.minecraft.nfc.device;

import javax.smartcardio.Card;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.TerminalFactory;
import java.util.List;

/**
 * ***********************************************
 * Created by Sola on 2014/6/25.
 * Don't modify this source without my agreement
 * ***********************************************
 */
public class ACR122 {

	private static CardTerminal terminal;

	public static void initTerminal() throws Exception {
		TerminalFactory factory = TerminalFactory.getInstance("PC/SC", null);
		System.out.println(factory);

		List<CardTerminal> terminals = factory.terminals().list();
		System.out.println("Terminals: " + terminals);
		if (terminals.isEmpty()) {
			throw new Exception("No card terminals available");
		}
		terminal = terminals.get(0);
	}

	public static Card getAvailableCard() throws CardException {
		return getAvailableCard(0);
	}

	public static Card getAvailableCard(long timeout) throws CardException {
		terminal.waitForCardPresent(timeout);
		return terminal.connect("*");
	}

	public static void waitForAbsent(long timeout) throws CardException {
		terminal.waitForCardAbsent(timeout);
	}

}
