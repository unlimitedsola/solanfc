package cn.net.minecraft.nfc.test;

import cn.net.minecraft.nfc.SolaNFC;
import cn.net.minecraft.nfc.calculator.FoodCalculator;
import cn.net.minecraft.nfc.calculator.WaterCalculator;
import cn.net.minecraft.nfc.data.Block;
import cn.net.minecraft.nfc.data.Key;
import cn.net.minecraft.nfc.device.ACR122;
import cn.net.minecraft.nfc.protocol.MifareClassic;
import cn.net.minecraft.nfc.storage.DataIO;
import org.junit.Test;

import javax.smartcardio.*;

import java.nio.charset.StandardCharsets;

import static cn.net.minecraft.nfc.util.ByteArrayUtil.bytesToHex;
import static cn.net.minecraft.nfc.util.ByteArrayUtil.hexToBytes;

/**
 * ***********************************************
 * Created by Sola on 2014/6/15.
 * Don't modify this source without my agreement
 * ***********************************************
 */
public class TestClass {

	final static int IOCTL_SMARTCARD_ACR122_ESCAPE_COMMAND = 0x003136B0;

	public static void main(String[] args) throws Exception {
		ACR122.initTerminal();
		Card card = ACR122.getAvailableCard();
		MifareClassic m1 = new MifareClassic(card);

		m1.authBlock(Key.genDefault(), 0, MifareClassic.KEY_A);
		byte[] UID = m1.readBlock(0).getData();
		System.out.println("Card UID: " + bytesToHex(UID));

		byte[] keyA = WaterCalculator.calculateKey(UID);
		System.out.println("Calculated KeyA: " + bytesToHex(keyA));
		m1.authBlock(Key.genDefault(), 48, MifareClassic.KEY_A);
		//m1.authBlock(keyA, 48, MifareClassic.KEY_A);
		Block data = new Block(hexToBytes("01204500271053F1FFFFFFFFFFFFFFFF"));
		m1.writeBlock(48, data);
		System.out.println(bytesToHex(m1.readBlock(48).getData()));
		byte[] control = m1.readBlock(51).getData();
		System.arraycopy(keyA, 0, control, 0, 6);
		System.out.println("New Control Block Data: " + bytesToHex(control));
		m1.writeBlock(51, new Block(control));
		System.out.println(bytesToHex(m1.readBlock(51).getData()));
		card.disconnect(true);
		Thread.sleep(1000);
	}

	@Test
	public void testUID() throws Exception {
		ACR122.initTerminal();
		Card card = ACR122.getAvailableCard();
		MifareClassic m1 = new MifareClassic(card);
		System.out.println(bytesToHex(m1.getUID()));
	}

	@Test
	public void testReadKey() throws Exception {
		SolaNFC.DEBUG_MODE = true;
		ACR122.initTerminal();
		Card card = ACR122.getAvailableCard();
		MifareClassic m1 = new MifareClassic(card);
		m1.authBlock(Key.genDefault(), 0, MifareClassic.KEY_B);
		byte[] UID = m1.readBlock(0).getData();
		System.out.println("Card UID: " + bytesToHex(UID));
	}

	@Test
	public void testKeyStore() {
		SolaNFC.DEBUG_MODE = true;
		System.out.println(DataIO.byStored("mykeys.txt"));
	}

	@Test
	public void testLED() throws Exception {
		SolaNFC.DEBUG_MODE = true;

		ACR122.initTerminal();
		Card card = ACR122.getAvailableCard();
		MifareClassic m1 = new MifareClassic(card);
		m1.transmitRaw(new byte[]{(byte) 0xFF, 0x00, 0x40, (byte) 0b00000000, 0x04, 1, 2, 10, 1});
		//m1.transmitRaw(new byte[]{(byte) 0xFF,0x00,0x40, (byte) 0b00011010,0x04,14,0,1,1});
		//m1.transmitRaw(new byte[]{(byte) 0xFF,0x00,0x40, (byte) 0b10111101,0x04,10,5,5,1});
		Thread.sleep(3000);
	}

	@Test
	public void calculateBalance() {
		System.out.printf("%x\n", WaterCalculator.calculateCheckBit(0xFFFF));
		byte[] data = hexToBytes("01204500271053F1FFFFFFFFFFFFFFFF");
		System.out.printf("%.2f\n", WaterCalculator.calculateBalance(data) / 100D);
	}

	@Test
	public void testBlock() throws Exception {
		SolaNFC.DEBUG_MODE = true;
		ACR122.initTerminal();
		Card card = ACR122.getAvailableCard();
		MifareClassic m1 = new MifareClassic(card);
		m1.authBlock(Key.genDefault(), 0, MifareClassic.KEY_A);
		System.out.println("m1.readBlock(0).data = " + bytesToHex(m1.readBlock(0).getData()));
//		m1.authBlock(Key.genDefault(), 1, MifareClassic.KEY_A);
//		m1.writeBlock(1, new Block("01204500271053F1FFFFFFFFFFFFFFFF"));
		m1.writeBlock(0, Block.genDefault());
//		m1.authBlock(Key.genDefault(), 1, MifareClassic.KEY_A);
		System.out.println("m1.readBlock(0).data = " + bytesToHex(m1.readBlock(0).getData()));
	}

	@Test
	public void testHalt() throws Exception {
		SolaNFC.DEBUG_MODE = true;
		ACR122.initTerminal();
		Card card = ACR122.getAvailableCard();
		MifareClassic m1 = new MifareClassic(card);
		CardChannel channel = card.getBasicChannel();
		transmit(card, "FF00000008D408630200630300");
		transmit(card, "FF00000006D442500057CD");
		transmit(card, "FF00000005D408633D07");
		transmit(card, "FF00000003D44240");
		transmit(card, "FF00000005D408633D00");
		transmit(card, "FF00000003D44243");
		transmit(card, "FF00000008D408630280630380");
		transmit(card, "FF00000015D44001A00012345678080804004659255849102302");
	}

	@Test
	public void testSetUID() throws Exception {
		SolaNFC.DEBUG_MODE = true;
		ACR122.initTerminal();
		Card card = ACR122.getAvailableCard();
		MifareClassic m1 = new MifareClassic(card);
		//7143C446B00804004659255849102302
		//12345678080804004659255849102302
		//22222222222222222222222222222222
		//E5: 718390A5C708040001245C80D6A7AF1D
		Block uid = new Block("3e0aa820bc080400018d56978c86d51d".toUpperCase());
		m1.modifyUID(uid);
		card.disconnect(true);
		Thread.sleep(2000);
		card = ACR122.getAvailableCard();
		m1 = new MifareClassic(card);
		m1.authBlock(Key.genDefault(), 0, MifareClassic.KEY_A);
		System.out.println("m1.readBlock(0).data = " + bytesToHex(m1.readBlock(0).getData()));
//		Block keyBlock = new Block("452380715916FF078069452380715916");
//		m1.writeBlock(3, keyBlock);
	}

	@Test
	public void testUIDXor() {
		byte[] data = hexToBytes("88888888080804004659255849102302");
		data[4] = (byte) (data[0] ^ data[1] ^ data[2] ^ data[3]);
		System.out.println(bytesToHex(data));
	}

	@Test
	public void testDJMAXCard() {
		System.out.println(bytesToHex("FEDY".getBytes(StandardCharsets.US_ASCII)));
	}

	@Test
	public void testHex() {
		System.out.println(bytesToHex(hexToBytes(" 00 88 99 9 7 7 7 89 7987 ")));
	}

	@Test
	public void testFoodKeyA() {
		System.out.println(bytesToHex(FoodCalculator.calculateKey(hexToBytes("23333333"))));
	}

	public void transmit(Card card, String hex) throws CardException {
		byte[] cmd = hexToBytes(hex);
		System.out.println(hex);
		CommandAPDU commandAPDU = new CommandAPDU(cmd);
		ResponseAPDU response = card.getBasicChannel().transmit(commandAPDU);
		System.out.println(bytesToHex(response.getBytes()));
		//byte[] response = card.transmitControlCommand(IOCTL_SMARTCARD_ACR122_ESCAPE_COMMAND, cmd);
		//System.out.println(bytesToHex(response));
	}

}
